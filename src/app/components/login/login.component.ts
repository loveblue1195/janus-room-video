import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  join = { username: '', token: '' }
  constructor(
    private _router: Router
  ) { }

  ngOnInit(): void {
  }

  joinRoomJanus() {
    localStorage.setItem('username', this.join.username)
    localStorage.setItem('janusToken', this.join.token)
    this._router.navigate(['/round-table'])
  }
}
