import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LiveStremingService } from 'src/app/services/live-streming.service';
import { RoundTableService } from 'src/app/services/round-table.service';

@Component({
  selector: 'app-round-table',
  templateUrl: './round-table.component.html',
  styleUrls: ['./round-table.component.scss']
})
export class RoundTableComponent implements OnInit {
  roomId = 1234
  username = ''
  isJoin = false
  shared = false;
  live = false
  constructor(
    private _liveService: LiveStremingService,
    private _roundTableService: RoundTableService,
    private _route: Router,
    private _router: ActivatedRoute
  ) {
    this._router.queryParams.subscribe(data => {
      console.log(data);
      if (data.token && data.username === 'panelist_bot') {
        localStorage.setItem('janusToken', data.token)
        localStorage.setItem('username', data.username)
        this.joinRoom()
        this.handlerJanusRoom()
      }
    })
  }

  ngOnInit(): void {
    this.joinRoom()
    this.handlerJanusRoom()
  }

  handlerJanusRoom() {
    this._roundTableService.joinedRoom.subscribe(data => {
      console.log('data', data);
      this.isJoin = data.isJoin
    })
  }

  joinRoom() {
    this.username = localStorage.getItem('username')
    const janusToken = localStorage.getItem('janusToken')
    this._liveService.connectWebsocket(janusToken)
    this._roundTableService.connectRoom(this.roomId, this.username, janusToken)
  }

  localToggleMuteAudio() {
    this._roundTableService.toggleAudio()
  }

  localToggleMuteVideo() {
    this._roundTableService.toggleVideo()
  }

  quitRoom() {
    this._roundTableService.removeRoom()
    this._route.navigate(['/'])
  }

  shareScreen() {
    this._roundTableService.shareScreen()
    this._roundTableService.isShareScreen()
    this.shared = true
  }
  stopShare() {
    this._roundTableService.stopShareScreen()
    this.shared = false
  }

  liveStream() {
    this._liveService.startRecording()
    this.live = true
  }

  stopStream() {
    this._liveService.stopRecordStream()
    this.live = false
  }
}
