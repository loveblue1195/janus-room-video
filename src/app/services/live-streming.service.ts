import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { environment } from 'src/environments/environment';
// declare var FFmpeg
import { createFFmpeg, fetchFile } from '@ffmpeg/ffmpeg';
// const { createFFmpeg, fetchFile } = FFmpeg;

@Injectable({
  providedIn: 'root'
})
export class LiveStremingService {
  ffmpeg
  ws: WebSocket
  recorder;
  stopCapture = new BehaviorSubject({ stopCapture: false })
  recordingData = []
  constructor() {
    this.ffmpeg = createFFmpeg({ log: true });
  }

  connectWebsocket(token) {
    const url = `${environment.wss}/rtmp?key=livestream&token=${token}&useragent=chorme`
    this.ws = new WebSocket(url)
    this.ws.addEventListener('open', () => {
      console.log('socket connected');
    })
    this.ws.addEventListener('close', () => {
      console.log('socket connect closed');
    })
    this.ws.addEventListener('error', (e) => {
      console.log('socket error:', e);
    })
    // this.ws.addEventListener('')
  }

  /**
  * Mixes multiple audio tracks and the first video track it finds
  * */
  mixerStream(stream1, stream2) {
    const ctx = new AudioContext();
    const dest = ctx.createMediaStreamDestination()
    if (stream1.getAudioTracks().length > 0)
      ctx.createMediaStreamSource(stream1).connect(dest)
    if (stream2.getAudioTracks().length > 0)
      ctx.createMediaStreamSource(stream2).connect(dest)

    let tracks = dest.stream.getTracks()
    tracks = tracks.concat(stream1.getVideoTracks()).concat(stream2.getVideoTracks())
    return new MediaStream(tracks)
  }

  /**
   * Start record streaming
   */
  async startRecording() {
    let gumStream, gdmStream;
    let recordingData = [];
    console.log('love');
    try {
      gumStream = await navigator.mediaDevices.getUserMedia({ video: false, audio: true })
      // @ts-ignore
      gdmStream = await navigator.mediaDevices.getDisplayMedia({ video: { displaySurface: 'browser' }, audio: { channelCount: 2 } })
    } catch (e) {
      console.log('capture failure', e);
      return
    }

    console.log(gumStream);

    const recordStream = gumStream ? this.mixerStream(gumStream, gdmStream) : gdmStream;
    // @ts-ignore
    this.recorder = new MediaRecorder(recordStream, { mimeType: 'video/webm;codecs=h264', videoBitsPerSecond: 3 * 1024 * 1024 })
    this.recorder.ondataavailable = e => {
      if (e.data && e.data.size > 0) {
        // console.log('live streaming packet', e);
        this.ws.send(e.data)
        this.recordingData.push(e.data);
      }
    }

    this.recorder.onStop = () => {
      recordStream.getTracks().forEach(track => track.stop());
      gumStream.getTracks().forEach(track => track.stop())
      gdmStream.getTracks().forEach(track => track.stop())
      this.ws.close()
    }

    recordStream.addEventListener('inactive', () => {
      console.log('Capture stream inactive');
      // stop capture
      this.stopCapture.next({ stopCapture: true })
      this.recorder.stop()
    })

    this.recorder.start(1000)
    console.log("started recording");
  }

  /**
   * Stop Record streaming
   */
  async stopRecordStream() {
    this.recorder.stop()
    console.log(this.ffmpeg.isLoaded());
    // @ts-ignore
    this.transcode(new Uint8Array(await (new Blob(this.recordingData)).arrayBuffer()));

  }


  getFilename() {
    const now = new Date();
    const timestamp = now.toISOString();
    const room = new RegExp(/(^.+)\s\|/).exec(document.title);
    if (room && room[1] !== "")
      return `${room[1]}_${timestamp}`;
    else
      return `recording_${timestamp}`;
  }

  transcode = async (webcamData) => {
    // const message = document.getElementById('message');
    const name = 'record.webm';
    // message.innerHTML = 'Loading ffmpeg-core.js';
    await this.ffmpeg.load();
    // message.innerHTML = 'Start transcoding';
    this.ffmpeg.FS('writeFile', name, await fetchFile(webcamData));
    await this.ffmpeg.run('-i', name, '-vcodec', 'copy', '-qscale', '0', 'output.mp4');
    // message.innerHTML = 'Complete transcoding';
    const data = this.ffmpeg.FS('readFile', 'output.mp4');

    // const video = document.getElementById('output-video');
    // video.src = URL.createObjectURL(new Blob([data.buffer], { type: 'video/mp4' }));
    const blob = new Blob([data.buffer], { type: 'video/mp4' })
    this.downfile(blob)
  }

  downfile(blob) {
    const url = window.URL.createObjectURL(blob);
    const a = document.createElement('a');
    a.style.display = 'none';
    a.href = url;
    a.download = `${this.getFilename()}.mp4`;
    document.body.appendChild(a);
    a.click();
    setTimeout(() => {
      document.body.removeChild(a);
      window.URL.revokeObjectURL(url);
      console.log(`${a.download} save option shown`);
    }, 100);
  }
}
