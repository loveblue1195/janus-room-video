export const environment = {
  production: true,
  wss: 'wss://forward-livestream.panelist.com',
  wssJanus: 'wss://call.panelist.com:8188'
};
